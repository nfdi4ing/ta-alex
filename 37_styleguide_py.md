---
#This header contains the meta-data (machine readable)
title: 'Mini Styleguide für Python'
status: in progress #planned, paused , in progress, final
task: [A-2-1, A-Y-Y] # includes measure
is_published: No # (Was this project published somwhere else?)
internal: No # (internal = no publication on website)
tags: [Alex, NFDI4ing,
  modular software, Python, Deutsch]
  #better more than less. (used to connect contents)
statusreport_ids: '-' # ID numbers used in NFDI4ing Status Report (Statusbericht) that this article relates to
authors:
  - name: Michaela Leštáková
    corresponding_author: yes
    orcid: 0000-0002-5998-6754 # please use ORCID
    affiliation: 1
  - name: Manuela Richter
    orcid: 0000-0003-1060-2622
    affiliation: 1
affiliations:
 - name: Chair of Fluid Systems
   index: 1
date: 2022-31-03 # start date
---

# Mini Styleguide für Python

*An English version will follow.*

:exclamation: Pflicht

:grey_exclamation: Optional

## Python Language Rules

### Lint
Unter Linting versteht man das Durchführen einer statischen Code-Analyse. Diese soll sicherstellen, dass der Code einheitlich formatiert und somit von gewisser Qualität ist. Wenn man alleine an einem Projekt arbeitet, mag dies nicht von großer Bedeutung sein, aber für die Arbeit in einem Team ist dies unerläßlich. Denn nur so wird sichergestellt, dass alle Teammitglieder einheitlichen Code erzeugen. [Quelle](https://bodo-schoenfeld.de/python-linting-flake8-einrichten/)

Wir verwenden den Linter `flake8`, welcher die PEP8-Standards überprüft.

:exclamation: Installiere `flake8` in deinem conda oder pip environment mit:

```bash
conda install flake8
```
oder 
```bash
pip install flake8
```
:exclamation: Aktiviere den Linter in deinem Editor, z.B. im VS Code:
- öffne `Settings`
- tippe "linting" ein
- aktiviere die folgenden Felder:
    - Python > Linting: Enabled
    - Python > Linting: Flake8 Enabled
    - Python > Linting: Lint on Save

:exclamation: *Die maximale line length soll 120 nicht überschreiten!*
Die Zeilenlänge kannst du unter `Settings` --> `Python` --> `Flake8 Args` anpassen, indem du auf `Add Item` clickst und `--max-line-length=120` eingibst. 

Danach soll in deinem Editor angezeigt werden, wenn du etwas falsch formattierst, zum Beispiel:

<img src="flake_example.PNG" width="50%" title="Beispiel für Flake8." alt="Figure broken">

### Imports
:exclamation: Imports sollen immer **gruppiert** im obersten Teil eines Moduls genannt werden, direkt nach Modul-docstrings und -kommentaren. 

```python
# Correct:
import os
import sys
```
```python
# Wrong:
import sys, os
```
It’s okay to say this though:
```python
# Correct:
from subprocess import Popen, PIPE
```
Mehr zu Imports: https://peps.python.org/pep-0008/#imports

## Aufbau von Code
- Mainskript
- Module mit Klassen und Funktionen
- Mainskript ruft Module auf

## Python Style Rules

### Code-Dokumentation
:exclamation: Dokumentiere deinen Code direkt, wenn du ihn schreibst. 
Im wesentlichen gibt es zwei Wege, wie man Code dokumentieren kann: **docstrings** und **Kommentare**.

Ein **docstring** ist ein `string`, welches eine Klasse, eine Funktion oder ein Modul beschreibt. Beispiel (docstring in rot):

```python
def multiply(a, b):
    """Multiplies the floats a and b.

    Args:
        a (float): First float
        b (float): Second float

    Returns:
        float: result of the multiplication of a and b
    """    
    return a * b
```

Ein **Kommentar** kann an einem beliebigen Ort zum Beispiel innerhalb einer Funktion oder in einem Skript stehen und hilft den Entwickler*innen, den Code besser nachvollziehen zu können. Beispiel (Kommentar in grau):
```python
# multiply two floats
a, b = 2.5, 3.0
product = multiply(a, b)
```
### Gute docstrings schreiben
Um gute docstrings zu schreiben, solltest du dich an den folgenden Punkten halten:

:exclamation: **Generiere die docstrings automatisiert.**
- Installiere eine Erweiterung für die automatisierte Generierung von docstrings (VS Code: `autoDocstring - Python Docstring Generator` von Nils Werner). 
- Wähle das docstring Format `google` aus (VS Code: clicke auf `Settings` --> tippe `docstring` ein --> setze `Auto Docstring: Docstring Format` auf `google`)
- danach kannst du an einem geeigneten Ort in deinem Code mit Rechtsklick --> `Generate Docstring` ein neues, vorausgefülltes docstring einlegen

:exclamation: **Fülle die vorausgefüllte docstrings VOLLSTÄNDIG aus.**
- es ist sehr wichtig, die docstrings vollständig auszufüllen
- mind. 1 Satz, um die Klasse oder Funktion oder das Modul zu beschreiben
- alle Argumente beschreiben
- alle `Type`s von Argumenten angeben
- `return` beschreiben

:exclamation: **Auf einheitliche, konforme Beschreibungen achten**
- um docstrings einheitlich zu schreiben, installiere einen docstring-Linter in dein Conda oder pip environment:
```bash
conda install -c conda-forge flake8-docstrings
```
oder 
```bash
pip install flake8-docstrings
```
In VS Code, öffne Settings --> tippe `flake8` ein und füge `--docstring-convention=google` in das Feld `Python > Linting: Flake8 Args` ein.

Jetzt hebt flake8 hervor, wenn du einen docstring falsch formuliert hast. 

Ein Beispiel für eine gute Formulierung von docstrings ist:
```python
def find_largest_distance(point, polygon):
    """Finds the largest distance between a point and the edges of a polygon.

    Args:
        point (shapely.geometry.Point): shapely point object
        polygon (shapely.geometry.Polygon): shapely polygon object

    Returns:
        float: the largest distance between a point and the edges of a polygon
    """
    distance_list = np.array([])
    for poly_point in list(zip(*polygon.exterior.coords.xy)):
        distance = point.distance(Point(poly_point))
        distance_list = np.append(distance_list, distance)
    max_distance = max(distance_list)
    return max_distance
```
Warum?
- [x] kurze und prägnante Beschreibung der Funktion
- [x] fängt mit einem Verb in 3. Person an
- [x] `type` von Args und return sind angegeben
- [x] Args und return sind beschrieben

:grey_exclamation: **Mehr zu google-docstrings:** https://google.github.io/styleguide/pyguide.html#38-comments-and-docstrings

### :exclamation: Namenskonvention für Variablen, Klassen, Modulen etc. 
<table rules="all" border="1" summary="Guidelines from Guido's Recommendations"
       cellspacing="2" cellpadding="2">

  <tr>
    <th>Type</th>
    <th>Public</th>
    <th>Internal</th>
  </tr>

  <tr>
    <td>Packages</td>
    <td><code>lower_with_under</code></td>
    <td></td>
  </tr>

  <tr>
    <td>Modules</td>
    <td><code>lower_with_under</code></td>
    <td><code>_lower_with_under</code></td>
  </tr>

  <tr>
    <td>Classes</td>
    <td><code>CapWords</code></td>
    <td><code>_CapWords</code></td>
  </tr>

  <tr>
    <td>Exceptions</td>
    <td><code>CapWords</code></td>
    <td></td>
  </tr>

  <tr>
    <td>Functions</td>
    <td><code>lower_with_under()</code></td>
    <td><code>_lower_with_under()</code></td>
  </tr>

  <tr>
    <td>Global/Class Constants</td>
    <td><code>CAPS_WITH_UNDER</code></td>
    <td><code>_CAPS_WITH_UNDER</code></td>
  </tr>

  <tr>
    <td>Global/Class Variables</td>
    <td><code>lower_with_under</code></td>
    <td><code>_lower_with_under</code></td>
  </tr>

  <tr>
    <td>Instance Variables</td>
    <td><code>lower_with_under</code></td>
    <td><code>_lower_with_under</code> (protected)</td>
  </tr>

  <tr>
    <td>Method Names</td>
    <td><code>lower_with_under()</code></td>
    <td><code>_lower_with_under()</code> (protected)</td>
  </tr>

  <tr>
    <td>Function/Method Parameters</td>
    <td><code>lower_with_under</code></td>
    <td></td>
  </tr>

  <tr>
    <td>Local Variables</td>
    <td><code>lower_with_under</code></td>
    <td></td>
  </tr>

</table>

### übliche Variablennamen
- Liste mit Standard-Variablennamen
- SpellCheck

### Whitespaces
:exclamation: Verwende automatisierte Entfernung von Whitespaces nach Save: `Settings` --> `Files`--> `Trim Trailing Whitespace`

:grey_exclamation: Um Whitespaces hervorzuheben, kannst du die Erweiterung `Highlight Trailing White Spaces` (Yves Baum) verwenden. Dies wird dir helfen, sauberen Code zu schreiben.


Themenspeicher: Design Patterns, Code - Struktur, DLR - Leitfaden, PEP 8, PEP 257 docstring
