---
#This header contains the meta-data (machine readable)
title: 'Documentation of Action Items'
status: 'in progress'
task: [] # includes measure
is_published: No # Yes, No (Was this project published somwhere else?)
internal: No # Yes, No (internal = no publication on website)
tags: [Alex, NFDI4ing, FAIR, Documentation, Action Items]

statusreport_ids: [35]  # ID numbers used in NFDI4ing Status Report (Statusbericht) that this article relates to
authors:
  - name: Jan Lemmer
    orcid: 0000-0002-0638-1567
    affiliation: 1 
  - name: Martin Hock
    corresponding_author: true
    orcid: 0000-0001-9917-3152
    affiliation: 1
affiliations:
 - name: Chair of Fluid Systems, TU Darmstadt
   index: 1
date: 2021-10-14 # start date
date-updated: 2022-07-13
---
# Documentation of Action Items

## Summary

To document the work done in the archetype Alex, a system was designed to implement a unified documentation for the working status of action item.

**Measures:**
This action item does not fit into any of the archetype's measures, as its use is to lessen the beaurocratic burden, and enable a FAIR documentation of the work done within the measures and tasks.

## Description

The number of participant and multitude of use cases made it necessary to divide the Measures and Tasks of the archetype Alex into even smaller units we call Action Items. These action items connect the use cases of a participant with specific tasks. To unify the documentation each action item's status, and ease the publication with FAIR means, a system to collect the documentation following a template was needed.
The documentation shall progress along the work on an action item. A specific version can be accessed and referenced via Gitlab, but the documents are meant to be updated continously - even after the work on an action item has finished.

The software stack used contains: GitLab for data storage, sharing and collaboration. GitLab's CI/CD for automated processing and compiling and publishing via GitLab Pages. The articles are written in easily accessible markdown format, enhanced by yaml front matter metadata. Hugo is the static site builder converting the markdown into a website and specific Hugo Go code snippets bring capabilities that markdown can not cover.



### Status

#### planned activities

- Processing of Metadata to sort the action items and create sections on shared properties. (Q3/22)
- Subdomain for easier access and sharing of the [public website](https://ta_alex.pages.rwth-aachen.de/action-items/)'s URL (Q3/22)
- Evaluating the creation of further material (poster) 

#### in progress activities

- Creation of documentation for action items in progress (ongoing)
- Usage of the articles in NFDI4INg's status reports (ongoing)

#### completed activities

- Creation of a Gitlab repository (limited to members of the Task Area) to host the documentation documents, supplementing material and discussion via the issue system.
- Creation of a template document, including machine readable metadata in YAML Frontmatter, as well as full text descriptions and graphics in Markdown.
- Introduction of the system to all members of the Archetype Alex.
- Creation of a usage guide.
- Drafting a workflow that enables (automatic) publication of documentations on website, following the MetaData in the documents either publicly or internally.
- Minimal viable product for publication of the documents, relying on manual copy of items into a public Gitlab repository to enable viewing of the documents (Q4/21)
- Implementation of an internal Gitlab Pages [website](https://ta_alex.pages.rwth-aachen.de/documentation/), listing each action item as article (Q1/22)
- Implementation of a CI/CD Pipeline to process documentation items depending on FrontMatter Metadata per file (Q2/22)
- Implementation of a *public* Gitlab Pages [website](https://ta_alex.pages.rwth-aachen.de/action-items/), automatically publishing the action items marked as not internal (Q2/22)


## Results

- Public repository: https://git.rwth-aachen.de/ta_alex/action-items
- Template documentation: [00_template.md](https://ta_alex.pages.rwth-aachen.de/action-items/template.md)
- First documentations (see repository)
- Internal (preview) [GitLab Pages](https://ta_alex.pages.rwth-aachen.de/documentation/) website
- Public [GitLab Pages](https://ta_alex.pages.rwth-aachen.de/action-items/) website

### Lessons Learned/ Recommendations

- File and folder structures that are easy to acess for  users do not translate to easy access for applications.
- Good metadata terminology is not common yet and needs to be pulled from other areas/sources (literature).
- Starting with a less-satisfying product can yield better results than striving for high quality to launch with.

### Publication(s)
See [repository](https://git.rwth-aachen.de/ta_alex/action-items/)
## Acknowledgements

## References
The NFDI4ING's *[Knowledge Base](https://knowledge-base.nfdi4ing.de/)* is using the same software stack and explains basic usage of CI/CD Pipelines, git, GitLab and Hugo.  
Code snippets for Hugo and the tag system are based on this work.   
<!--- References to external knowledge --->
