---
#This header contains the meta-data (machine readable)
title: 'meaningful title' # Title for the menu / overview page
# menutitle: 'Short title' # Title to be shown in the menu
status: # [planned, on hault, in progress, final,...]
task: [A-X-X, A-Y-Y] # includes measure
is_published: # Yes, No (Was this project published somewhere else?)
internal: # Yes, No (internal = no publication on public website)
tags: [Alex, NFDI4ing,
  Measure 1, Measure 2,
  Subject 1, Subject 2, Language]
  #better more than less. (used to connect contents)
statusreport_ids: '-' # ID numbers used in NFDI4ing Status Report (Statusbericht)
# that this article relates to. Use '-' instead of no entry/null/0. DO NOT COMMENT OUT!
authors:
  - name: Max Mustermann
    corresponding_author: yes
    orcid: 0000-0002-0638-1567 # please use ORCID
    affiliation: 1 # (Multiple affiliations must be quoted)
  - name: Author 2
    affiliation: 2
affiliations:
 - name: Example Institute 
   index: 1
   # ROR: 
   # website: 
 - name: Example Institute 2
   index: 2
date: 2021-10-16 # Start date
# bibliography: paper.bib #if available
---

# Title of the Action Item

## Summary
<!--- 2-3 sentences --->
<!--- What is the goal of the action item? --->
**Measures:**
<!--- please describe, how the action item refers to the measures --->

## Description
<!--- Brief, generally understandable presentation of the work done and
the progress made  --->
<!--- How does it fit in to the task? --->
### Status
<!--- list the activities, the results have a own section --->

<!--- planned and completed steps --->
#### planned activities
<!--- please add a calender quarter if possible --->
- Example activity 1 (Q2/21)
- Completion of this action item in Q1/22
#### in progress activities
A full text is also possible
#### completed activities
<!--- if completed, then list follow-up activities and action items --->

<!---  Figures
Figures can be included like this:
![Caption for example figure.\label{fig:example}](figure.png)
and referenced from text using \autoref{fig:example}.

**Figures**  
Pictures and other additional files go into a lower case folder with the same name as the markdown file.
For the pictures to show in the GitLab Markdown preview they need to have the folder added to the filepath,  
but to work for hugo the folder can __not__ be added.  JPG and PNG files work out of the box, for SVG and others more research is necessary (Hugo Doc and Theme Doc).


Figure sizes can be customized in a Gitlab compatible way using html-code:  
`<img src="tengyart-kSvpTrfhaiU-unsplash.jpg" width="25%" title="Title (label) of the figure" alt="Text to show if image is not rendered">` for %-scaling  
<img src="00_template/tengyart-kSvpTrfhaiU-unsplash.jpg" width="25%" title="Title (label) of the figure" alt="Text to show if image is not rendered">  
`<img src="tengyart-kSvpTrfhaiU-unsplash.jpg" width="500" title="Title (label) of the figure" alt="Text to show if image is not rendered">` for size in pixel.  
Height can be added if the ratio width/height is to be changed.  
For publication adjusting the URL also works, but won't adjust the size in Gitlab Preview  
`![Title (label)](figure.png?width=500px)`  

--->

## Results
<!--- Brief describtion of the project results achieved, based on activities described above --->

### Lessons Learned/ Recommendations
<!--- list noteable experiences and recommendations gained during the progress of this action item --->

### Publication(s)
<!--- Please list all publications (paper, software, conference presentations, materials etc.) based on the action item --->

## Acknowledgements

## References
<!--- References to external knowledge --->
