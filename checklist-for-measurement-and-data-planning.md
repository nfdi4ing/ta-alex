---

title: 'Checklist - Measurement and data planning for machine learning in assembly' # Title for the menu / overview page
# menutitle: 'ML-Checklist' # Title to be shown in the menu
status: 'final'
# task: [A-X-X, A-Y-Y] # includes measure
is_published: Yes
internal: No 
tags: [Alex, NFDI4ing, English]
statusreport_ids: '-' # ID numbers used in NFDI4ing Status Report (Statusbericht)
# that this article relates to. Use '-' instead of no entry/null/0
authors:
  - name: Christopher Schnur
    corresponding_author: yes
    orcid: 0000-0002-6844-0382 # please use ORCID
    affiliation: 1 
  - name: Steffen Klein
    corresponding_author: no
    affiliation: 1
  - name: Anne Blum
    corresponding_author: no
    orcid: 0000-0003-0994-4457 # please use ORCID
    affiliation: 3
affiliations:
 - name: Saarland University, Lab for Measurement Technology 
   index: 1
   # ROR: 
   # website: 
 - name: Centre for Mechatronics and Automation Technology gGmbH
   index: 2
 - name: Saarland University, Chair of Assembly Systems
   index: 3
date: 2023-01-21 
# bibliography: paper.bib #if available
---

# Checklist - Measurement and data planning for machine learning in assembly

In order to fully exploit the great potential of artificial intelligence or machine learning in industry, and especially in assembly, the quality of the data plays a decisive role. The checklist provided on the open repository zenodo is intended to support industrial users, in the acquisition of high-quality data in order to minimise the effort required for analysis and to increase the significance of the results. It serves to support the planning of a project for the use of machine learning on an existing assembly line (brownfield). The focus is therefore not on the acquisition of a new plant or the data planning for prototypes in product development. Nevertheless, the checklist can also provide orientation for these use cases. It pursues the approach of recording valid data as specifically as possible through the contribution of expert knowledge and making it available in a clear manner. Due to the high diversity of available machine learning methods, data analysis is only dealt with at a basic level within the scope of this document.

![Checklist](check.png)

At the beginning of each chapter, a short paragraph introduces the topic. This is followed by
the checkpoints to be ticked off. Basically, there are two types of checkpoints
must-checkpoints and best-practice-checkpoints. The must checkpoints are those that must be fullfilled in order to successfully complete the upcomming chapters. Best-practice checkpoints are those that are not essential for the basic processing of the sections, but which have proved to be very helpful in further optimisation.

![Checkpoint](checkpoints.png)

Furthermore, the checklist contains useful tips (recommendations for action) and hints
(things you should pay attention to) that support you in your work.

![symbol](tip.png)


The checklist can be downloaded by clicking the link below https://www.zenodo.org/record/7556876#.ZAtDkXbMJD9


Source of the original Checklist in German:
Christopher Schnur, Steffen Klein, & Anne Blum. (2022). Checkliste – Mess- und Datenplanung für das maschinelle Lernen in der Montage (Version 7). Zenodo. https://doi.org/10.5281/zenodo.6943476

